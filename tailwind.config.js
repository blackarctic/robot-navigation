module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    rotate: {
      0: "0deg",
      45: "45deg",
      90: "90deg",
      135: "135deg",
      180: "180deg",
      225: "225deg",
      270: "270deg",
      315: "315deg",
      360: "0deg",
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
