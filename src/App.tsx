import React from "react";
import { RecoilRoot } from "recoil";

import { useIsRobotOn, Display } from "./services/world";
import { useRunRobot } from "./services/robot";

const RunRobot: React.FC = () => {
  const [shouldRun, setShouldRun] = React.useState(false);
  const runRobot = useRunRobot();
  const runRobotTimeout = React.useRef<number | undefined>();

  React.useEffect(() => {
    const queueNextRun = () => {
      runRobotTimeout.current = window.setTimeout(() => {
        setShouldRun(true);
        queueNextRun();
      }, 50);
    };
    queueNextRun();

    return () => clearTimeout(runRobotTimeout.current);
  }, []);

  React.useEffect(() => {
    if (shouldRun) {
      runRobot();
      setShouldRun(false);
    }
  }, [runRobot, shouldRun]);

  return null;
};

const Logic: React.FC = () => {
  const isRobotOn = useIsRobotOn();

  if (!isRobotOn) {
    console.log("robot is off");
    return null;
  }

  return <RunRobot />;
};

const App: React.FC = () => {
  return (
    <RecoilRoot>
      <Logic />
      <Display />
    </RecoilRoot>
  );
};

export default App;
