import React from "react";
import {
  atom,
  useRecoilValue,
  useRecoilState,
  useSetRecoilState,
} from "recoil";
import _ from "lodash";

enum BoxType {
  "c" = "closed",
  "o" = "open",
  "s" = "start",
  "f" = "finish",
  "r" = "robot",
}

const boxTypeToColor: Record<BoxType, string> = {
  closed: "bg-gray-500",
  open: "bg-gray-100",
  start: "bg-green-200",
  finish: "bg-red-200",
  robot: "bg-blue-200",
};

export const directionDegrees = [0, 45, 90, 135, 180, 225, 270, 315] as const;
export type DirectionDegrees = typeof directionDegrees[number];

export const directionDegreesToName: Record<DirectionDegrees, string> = {
  0: "n",
  45: "ne",
  90: "e",
  135: "se",
  180: "s",
  225: "sw",
  270: "w",
  315: "nw",
};

const generateGrid = () => {
  const numOfRows = _.random(10, 20);
  const numOfCols = _.random(10, 20);
  const emptyGrid = new Array(numOfRows)
    .fill(undefined)
    .map(() => new Array(numOfCols).fill(undefined));

  const startRowIndex = _.random(1, numOfRows - 2);
  const startColIndex = _.random(1, numOfCols - 2);

  const finishWall = _.random(1, 4);
  const finishColIndex = (() => {
    if (finishWall === 1) {
      return 0;
    }
    if (finishWall === 2) {
      return numOfCols - 1;
    }
    return _.random(0, numOfCols - 1);
  })();
  const finishRowIndex = (() => {
    if (finishWall === 3) {
      return 0;
    }
    if (finishWall === 4) {
      return numOfRows - 1;
    }
    return _.random(0, numOfRows - 1);
  })();

  return emptyGrid.map((row, rowIndex) =>
    row.map((col, colIndex) => {
      const boxType = (() => {
        if (rowIndex === startRowIndex && colIndex === startColIndex) {
          return BoxType.s;
        }
        if (rowIndex === finishRowIndex && colIndex === finishColIndex) {
          return BoxType.f;
        }
        if (rowIndex === 0 || rowIndex === numOfRows - 1) {
          return BoxType.c;
        }
        if (colIndex === 0 || colIndex === numOfCols - 1) {
          return BoxType.c;
        }

        const isClosed = _.random(1, 4);
        if (isClosed === 1) {
          return BoxType.c;
        }

        return BoxType.o;
      })();

      return {
        boxType,
      };
    })
  );
};

const grid = generateGrid();

const startingRowIndex = grid.findIndex((row) =>
  row.some((item) => item.boxType === BoxType.s)
);
const startingColIndex = grid[startingRowIndex].findIndex(
  (item) => item.boxType === BoxType.s
);

const robotRowIndexAtom = atom({
  key: "robotRowIndexAtom",
  default: startingRowIndex,
});
const robotColIndexAtom = atom({
  key: "robotColIndexAtom",
  default: startingColIndex,
});
const robotDirectionAtom = atom<DirectionDegrees>({
  key: "robotDirectionAtom",
  default: 0,
});
const isRobotOnAtom = atom({
  key: "isRobotOnAtom",
  default: true,
});
const robotMovesCountAtom = atom({
  key: "robotMovesCountAtom",
  default: 0,
});
const isCompletedAtom = atom({
  key: "isCompletedAtom",
  default: false,
});

export const useRobotDirection = () => {
  return useRecoilValue(robotDirectionAtom);
};

export const useSetRobotDirection = () => {
  return useSetRecoilState(robotDirectionAtom);
};

export const useIsRobotOn = () => {
  return useRecoilValue(isRobotOnAtom);
};

export const useSetIsRobotOn = () => {
  return useSetRecoilState(isRobotOnAtom);
};

export type MoveRobotResult = "open" | "goal" | "hit";

export const useMoveRobot = (): (() => MoveRobotResult) => {
  const robotMovesCount = useRecoilValue(robotMovesCountAtom);
  const robotDirection = useRobotDirection();
  const [robotRowIndex, setRobotRowIndex] = useRecoilState(robotRowIndexAtom);
  const [robotColIndex, setRobotColIndex] = useRecoilState(robotColIndexAtom);
  const setIsCompleted = useSetRecoilState(isCompletedAtom);
  const setIsRobotOn = useSetRecoilState(isRobotOnAtom);
  const setRobotMovesCount = useSetRecoilState(robotMovesCountAtom);

  return React.useCallback(() => {
    setRobotMovesCount(robotMovesCount + 1);

    if (robotMovesCount > 1000) {
      console.log("battery exhausted");
      setIsRobotOn(false);
    }

    let nextRobotRowIndexDelta = 0;
    let nextRobotColIndexDelta = 0;

    const robotDirectionName = directionDegreesToName[robotDirection];
    if (robotDirectionName.startsWith("n")) {
      nextRobotRowIndexDelta = -1;
    }
    if (robotDirectionName.startsWith("s")) {
      nextRobotRowIndexDelta = 1;
    }
    if (robotDirectionName.endsWith("e")) {
      nextRobotColIndexDelta = 1;
    }
    if (robotDirectionName.endsWith("w")) {
      nextRobotColIndexDelta = -1;
    }

    const nextRobotRowIndex = robotRowIndex + nextRobotRowIndexDelta;
    const nextRobotColIndex = robotColIndex + nextRobotColIndexDelta;

    if (grid[nextRobotRowIndex][nextRobotColIndex].boxType === BoxType.c) {
      return "hit";
    }

    setRobotRowIndex(nextRobotRowIndex);
    setRobotColIndex(nextRobotColIndex);

    if (grid[nextRobotRowIndex][nextRobotColIndex].boxType === BoxType.f) {
      setIsCompleted(true);
      return "goal";
    }

    return "open";
  }, [
    robotMovesCount,
    robotDirection,
    robotColIndex,
    robotRowIndex,
    setRobotMovesCount,
    setIsCompleted,
    setRobotColIndex,
    setRobotRowIndex,
    setIsRobotOn,
  ]);
};

const Box: React.FC<{ rowIndex: number; colIndex: number }> = ({
  rowIndex,
  colIndex,
}) => {
  const robotDirection = useRecoilValue(robotDirectionAtom);
  const robotRowIndex = useRecoilValue(robotRowIndexAtom);
  const robotColIndex = useRecoilValue(robotColIndexAtom);

  let { boxType } = grid[rowIndex][colIndex];
  if (rowIndex === robotRowIndex && colIndex === robotColIndex) {
    boxType = BoxType.r;
  }

  const color = boxTypeToColor[boxType];

  return (
    <div
      className={`${color} relative flex-grow border-gray-300 border-2 transition-colors duration-300`}
    >
      {boxType === BoxType.r && (
        <div
          className={`absolute top-1/2 left-1/2 transform rotate-${robotDirection} -translate-x-1/2 -translate-y-1/2`}
        >
          ⬆
        </div>
      )}
    </div>
  );
};

const Status: React.FC = () => {
  const isCompleted = useRecoilValue(isCompletedAtom);
  const isRobotOn = useRecoilValue(isRobotOnAtom);
  const robotMovesCount = useRecoilValue(robotMovesCountAtom);

  let statusText = `Searching... (${robotMovesCount} moves)`;
  if (isCompleted) {
    statusText = `Finished in ${robotMovesCount} moves`;
  } else if (!isRobotOn) {
    statusText = `Battery exhausted after ${robotMovesCount} moves`;
  }
  return <div className="w-full text-center pt-3">{statusText}</div>;
};

const Grid: React.FC = () => {
  return (
    <>
      {grid.map((row, rowIndex) => (
        <div key={rowIndex} className="flex flex-grow">
          {row.map((col, colIndex) => (
            <Box
              key={`${rowIndex}-${colIndex}`}
              rowIndex={rowIndex}
              colIndex={colIndex}
            />
          ))}
        </div>
      ))}
    </>
  );
};

export const Display: React.FC = () => {
  return (
    <div className="container mx-auto h-screen flex justify-center items-center">
      <div className="flex flex-col w-full h-full p-10">
        <Grid />
        <Status />
      </div>
    </div>
  );
};
