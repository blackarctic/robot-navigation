import React from "react";
import _ from "lodash";

import {
  MoveRobotResult,
  DirectionDegrees,
  useMoveRobot,
  directionDegrees,
  directionDegreesToName,
  useSetRobotDirection,
  useSetIsRobotOn,
  useRobotDirection,
} from "./world";

const map: Record<number, Record<number, MoveRobotResult>> = {
  0: { 0: "open" },
}; // [row (y)][col (x)]
let prevX = 0;
let prevY = 0;

const getRandomDirection = (
  options: readonly DirectionDegrees[] = directionDegrees
) => options[_.random(0, options.length - 1)];

const getUnexploredDirection = (y: number, x: number) => {
  const unexploredDirections: DirectionDegrees[] = [];
  // n
  if (!map[y - 1] || !map[y - 1][x]) {
    unexploredDirections.push(0);
  }
  // ne
  if (!map[y - 1] || !map[y - 1][x + 1]) {
    unexploredDirections.push(45);
  }
  // e
  if (!map[y] || !map[y][x + 1]) {
    unexploredDirections.push(90);
  }
  // se
  if (!map[y + 1] || !map[y + 1][x + 1]) {
    unexploredDirections.push(135);
  }
  // s
  if (!map[y + 1] || !map[y + 1][x]) {
    unexploredDirections.push(180);
  }
  // sw
  if (!map[y + 1] || !map[y + 1][x - 1]) {
    unexploredDirections.push(225);
  }
  // w
  if (!map[y] || !map[y][x - 1]) {
    unexploredDirections.push(270);
  }
  // nw
  if (!map[y - 1] || !map[y - 1][x - 1]) {
    unexploredDirections.push(315);
  }

  if (unexploredDirections.length) {
    return getRandomDirection(unexploredDirections);
  }
  return undefined;
};

const getOpenDirection = (y: number, x: number) => {
  const openDirections: DirectionDegrees[] = [];
  // n
  if (map[y - 1] && map[y - 1][x] === "open") {
    openDirections.push(0);
  }
  // ne
  if (map[y - 1] && map[y - 1][x + 1] === "open") {
    openDirections.push(45);
  }
  // e
  if (map[y] && map[y][x + 1] === "open") {
    openDirections.push(90);
  }
  // se
  if (map[y + 1] && map[y + 1][x + 1] === "open") {
    openDirections.push(135);
  }
  // s
  if (map[y + 1] && map[y + 1][x] === "open") {
    openDirections.push(180);
  }
  // sw
  if (map[y + 1] && map[y + 1][x - 1] === "open") {
    openDirections.push(225);
  }
  // w
  if (map[y] && map[y][x - 1] === "open") {
    openDirections.push(270);
  }
  // nw
  if (map[y - 1] && map[y - 1][x - 1] === "open") {
    openDirections.push(315);
  }

  if (openDirections.length) {
    return getRandomDirection(openDirections);
  }
  return undefined;
};

const getEstimatedMapDimensions = () => {
  const yCoords = Object.keys(map).map(Number);
  const maxYCoord = _.max(yCoords);
  const minYCoord = _.min(yCoords);
  const height =
    _.isNumber(maxYCoord) && _.isNumber(minYCoord)
      ? maxYCoord - minYCoord + 1
      : 0;

  const widths = Object.values(map).map((col) => {
    const xCoords = Object.keys(col).map(Number);
    const maxXCoord = _.max(xCoords);
    const minXCoord = _.min(xCoords);
    const width =
      _.isNumber(maxXCoord) && _.isNumber(minXCoord)
        ? maxXCoord - minXCoord + 1
        : 0;
    return width;
  });
  const width = _.max(widths);

  return { width, height };
};

export const useRunRobot = () => {
  const moveRobot = useMoveRobot();
  const robotDirection = useRobotDirection();
  const setRobotDirection = useSetRobotDirection();
  const setIsRobotOn = useSetIsRobotOn();

  return React.useCallback(() => {
    const currY = prevY;
    const currX = prevX;

    const robotDirectionName = directionDegreesToName[robotDirection];

    let yDelta = 0;
    let xDelta = 0;

    if (robotDirectionName.startsWith("n")) {
      yDelta = -1;
    }
    if (robotDirectionName.startsWith("s")) {
      yDelta = 1;
    }
    if (robotDirectionName.endsWith("e")) {
      xDelta = 1;
    }
    if (robotDirectionName.endsWith("w")) {
      xDelta = -1;
    }

    const movingIntoY = currY + yDelta;
    const movingIntoX = currX + xDelta;

    const movingInto = map[movingIntoY] && map[movingIntoY][movingIntoX];

    const unexploredDirection = getUnexploredDirection(currY, currX);
    const openDirection = getOpenDirection(currY, currX);

    if (movingInto === "hit") {
      setRobotDirection(
        unexploredDirection || openDirection || getRandomDirection()
      );
      return;
    }

    if (movingInto === "open") {
      if (unexploredDirection) {
        setRobotDirection(unexploredDirection);
        return;
      }
    }

    const resultOfMove = moveRobot();

    switch (resultOfMove) {
      case "goal":
        setIsRobotOn(false);
        break;
      case "hit":
        map[movingIntoY] = map[movingIntoY] || {};
        map[movingIntoY][movingIntoX] = "hit";

        setRobotDirection(
          unexploredDirection || openDirection || getRandomDirection()
        );

        break;
      case "open":
        map[movingIntoY] = map[movingIntoY] || {};
        map[movingIntoY][movingIntoX] = "open";

        prevY = movingIntoY;
        prevX = movingIntoX;

        break;
      default:
        console.log("got unknown move result", resultOfMove);
        break;
    }

    const estimatedDims = getEstimatedMapDimensions();
    console.log(
      `guessing the map is ${estimatedDims.height} high x ${estimatedDims.width} wide`
    );
  }, [robotDirection, moveRobot, setRobotDirection, setIsRobotOn]);
};
